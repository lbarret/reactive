
from distutils.core import setup

setup(
    name             = 'Reactive',
    version          = '0.1.0',
    author           = 'Lionel Barret',
    author_email     = 'lionel.barret@gmail.com',
    packages         = ['reactive', 'reactive.tests'],
    scripts          = [],
    url              = 'http://pypi.python.org/pypi/Reactive/',
    license          = 'LICENSE.txt',
    description      = 'reactive programming types and value',
    long_description = open('README.txt').read(),
    install_requires = [ ],
    )
