from data_structures import tree
#from pprint import pprint as pp
from collections import defaultdict

class TypeValidator(object):
    def __call__(self, naked_instance):
        raise NotImplementedError

class BaseValidator(TypeValidator):
    def __init__(self, _class):
        self._class = _class
    def __repr__(self):
        return str(self._class)
    def __call__(self, naked_instance):
        #print "validating %s %s" %(naked_instance, self)
        return isinstance(naked_instance, self._class)

class _Type(tree.Node):

    def __init__(self, factory, type_validator=None, name=None, children=None, readable=True):
        self.factory        = factory
        self.type_validator = type_validator or BaseValidator(factory)
        self.name          = name or str(type(factory))
        self.readable      = readable
        tree.Node.__init__(self, children=children)

    def validate(self, naked_instance):
        return self.type_validator(naked_instance)

    def get_default(self, factory=None, **kw):
        factory = factory or self.factory
        v = self._get_default(factory, **kw)
        assert self.validate(v)
        return v

    def _get_default(self, factory, **kw):
        return factory(**kw)

    def __repr__(self):
        return str(self.factory)

    def is_union(self):
        return False

class _Class(_Type):
    def __init__(self, python_cls, **kw):
        if 'name' not in kw:
            kw['name'] = python_cls.__name__
        super(_Class, self).__init__(python_cls, **kw)

    def invariant(self):
        return all(isinstance(child, Name) for child in self.children)

    def _get_default(self, factory, deferred=[], **kw):
        assert self.invariant()
        kwargs = {}
        for child in self.children:
            if child.name in kw:
                kwargs[child.name] = kw[child.name] 
            elif child.name in deferred:
                kwargs[child.name] = child.get_default
            else:
                kwargs[child.name] = child.get_default()
        #pp(kw)
        return factory(**kwargs)

    def validate(self, naked_instance):
        assert self.invariant()
        # we need to split the if to shortcut most 
        # validation of recursive types
        if not self.type_validator(naked_instance):
            return False
        return all(child.validate(naked_instance) for child in self.children)

    def add_attr(self, name, rtype, mods=None):
        n = Name(name, mods)
        tree.connect(rtype, n) 
        tree.connect(n, self) 


class Name(tree.Node, tree.OneChildMixin):
    def __init__(self, name='anonymous', mods=None):
        self.name = name
        tree.Node.__init__(self)
        self.mods = mods if mods else []
    
    def extract(self, inst):
        return getattr(inst, self.name, None)

    def set(self, inst, value):
        setattr(inst, self.name, value)

    def __repr__(self):
        return str(self.name)

    def get_default(self, **kw):
        #pp("get_default %s" %self)
        ttype = self.get_only_child()
        return ttype.get_default(**kw)

    def validate(self, naked_instance):
        v = self.extract(naked_instance)
        return self.get_only_child().validate(v)


class List(_Type):
    def invariant(self):
        return all(isinstance(child, Index) for child in self.children)

    def __init__(self, factory=None, **kw):
        factory = factory or list
        super(List, self).__init__(factory, **kw)

    def is_multi_list(self):
        return len(self.children)==1 and self.children[0].multi

    def _get_default(self, factory, **kw):
        if self.children[0].multi:
            assert len(self.children) == 1
            return factory([])
        else:
            sorted_children = sorted(self.children, key = lambda c : c.name)
            return factory((c.get_default() for c in sorted_children))

    def add_idx(self, idx, rtype):
        i = Index(idx)
        tree.connect(rtype, i) 
        tree.connect(i, self) 
        return self

    def add_idxs(self, rtypes):
        for idx, rtype in enumerate(rtypes):
            self.add_idx(idx, rtype)
        return self


class Index(Name):
    def __init__(self, idx):
        assert (isinstance(idx, int) and idx>=0) or idx == "multi"
        self.multi = idx == 'multi'
        Name.__init__(self, idx) 

    def extract(self, inst, idx=None):
        assert (self.multi and idx is not None) or \
               (idx is None and not self.multi) 
        #==
        if self.multi : return inst[idx]
        else          : return inst[self.name]

    def set(self, inst, value, idx=None):
        assert not self.multi or idx is not None
        if not self.multi:
            inst[self.name] = value
        else:
            inst[idx] = value

    def validate(self, naked_instance):
        if self.multi:
            child = self.get_only_child()
            return all(child.validate(el) for el in naked_instance)
        else:
            return Name.validate(self, naked_instance)


class Union(_Type):
    def __init__(self,  default_idx=0, **kw):
        _Type.__init__(self, None, **kw)
        self.default_idx = default_idx

    def validate(self, naked_instance):
        return any( s.validate(naked_instance) for s in self.children)

    def _get_default(self, factory, **kw):
        return self.children[self.default_idx].get_default(**kw)

    def get_active(self, naked_instance):
        for s in self.children:
            if s.validate(naked_instance):
                return s
        assert False

    def __repr__(self):
        return '|'.join(map(str, self.children))

    def is_union(self):
        return True

def infer_iterable(iterable):
    assert all(type(el) in (int,float, str) for el in iterable) 
    ll = List(lambda x: iterable, type_validator=lambda x: isinstance(x, type(iterable)))
    ll.add_idxs([_Type(type(el)) for el in iterable])
    return ll

def infer_leaftype(leafvalue):
    assert type(leafvalue) in (int,float, str)
    _type = _Type(lambda : leafvalue, type_validator=lambda x: isinstance(x, type(leafvalue)))
    return _type

class ThisType(tree.Node):
    pass

This= ThisType()

class Nothing(object):
    def __json__(self):
        return "Nothing"

NothingType = _Type(Nothing)

class Promise(tree.Node):
    def __init__(self, name):
        super(Promise, self).__init__()
        self.name = name
        #assert name != "type"

    def __repr__(self):
        return "Promise:%s" %self.name

    #def pre_disconnect(self, child, parent):
        #print self, "disconnect from", parent
        ##assert False
        #tree.Node.pre_disconnect(self, child, parent)

    #def pre_connect(self, child, parent):
        #tree.Node.pre_connect(self, child, parent)
        #print self, "connect to", parent

class Scope(object):
    def __init__(self, name):
        self.name          = name
        self.type_registry = {}
        self.promises      = defaultdict(list)

    def Rtype(self, cls, ):
        if cls == "this":
            return This
        elif isinstance(cls, basestring):
            return self.get_type(cls)
        else:
            rtype =  getattr(cls, "rtype", None)
            return rtype if rtype else _Type(cls)

    def register(self, rtype, alias=None):
        type_name = rtype.name if not alias else alias
        assert self.type_registry.get(type_name) is None
        self.type_registry[type_name] = rtype
        #==
        promises = self.promises[type_name]
        for p in promises:
            assert len(p.parents) == 1
            assert isinstance(p.parents[0], (Name,Union))
            parent = p.parents[0]
            tree.disconnect(p, parent)
            tree.connect(rtype, parent)
        del self.promises[type_name]
        #==
        for _type in self.type_registry.values():
            for node in tree.dfs(_type):
                assert not (isinstance(node, Promise) and\
                            node.name == type_name)

    #for debug
    def check_promises_connected(self):
        for promises in self.promises.values():
            for p in promises:
                assert len(p.parents) == 1


    def get_type(self, type_name):
        _type = self.type_registry.get(type_name) 
        if not _type:
            _type = Promise(type_name)
            self.promises[type_name].append(_type)
        return _type

    def end_run(self):
        assert not self.promises
        #== 
        self.type_registry = {}
        self.promises = {}

scope = Scope("default")

Rtype = scope.Rtype

