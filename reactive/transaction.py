import _base as b
from data_structures import tree

class Transaction(object):
    def __init__(self, new, old=None, sender=None, target=None):
        assert old is not None or isinstance(self, (AppendItem, Init))
        if not sender or not target:
            debug_func()
        #==
        self.new    = new
        self.old    = old
        self.sender = sender
        self.target = target
        #==
        self.old_wrapped = None

    def __repr__(self):
        str_type = self.__class__.__name__ #str(type(self).re
        return "%s(new:%s old:%s sender:%s target:%s)" \
                %( str_type, self.new, self.old, self.sender, self.target.full_name())

    def wrap_ifn(self, rvalue):
        #if the new value is already wrapped, we just reparent
        #his children and add its parents to the current rvalue
        if hasattr(self.new, 'rnode'):
            rnode = self.new.rnode
            for p in rnode.parents[:]:
                tree.connect(rvalue, p) 
                tree.disconnect(rnode, p)
            for c in rnode.children[:]:
                c.unstable = True
                tree.connect(c, rvalue) 
                tree.disconnect(c, rnode)
                c.unstable = False
                c.invariant()
            self.new.rnode= rvalue

class Init(Transaction):

    def affect(self, listener):
        listener.tr_replace(self)


class Delete(Transaction):
    def __init__(self, idx, transaction, sender=None, target=None):
        self.idx         = idx
        self.transaction = transaction
        self.sender      = sender
        self.target      = target

    def affect(self, listener):
        listener.tr_delete(self)

    def __repr__(self):
        return "%s()" \
                %( str(type(self)))


class Replace(Transaction):
    def reverse(self):
        new, old = self.old, self.new
        return Replace(self.old, old=self.new, sender=self.sender, target=self.target)

    def proposed_value(self, naked_instance):
        assert naked_instance == self.old
        return self.new

    def do(self, rvalue):
        self.old_wrapped = rvalue.children[:]
        for c in rvalue.children[:]:
            tree.disconnect(c, rvalue)
        rvalue.naked_instance = self.new
        #==
        assert not rvalue.is_union()
        if hasattr(self.new, 'rnode'):
            rnode = self.new.rnode
            for p in rnode.parents[:]:
                tree.connect(rvalue, p) 
                tree.disconnect(rnode, p)
            for c in rnode.children[:]:
                c.unstable = True
                tree.connect(c, rvalue) 
                tree.disconnect(c, rnode)
                c.unstable = False
                c.invariant()
            self.new.rnode= rvalue
        else:
            for c in rvalue.rtype.children:
                vIndexes = b.wrap_name(c, self.new)
                for vIndex in vIndexes:
                    tree.connect(vIndex, rvalue)
        #==
        if isinstance(rvalue.naked_instance, list):
            assert len(rvalue.children)==len(rvalue.naked_instance)

    def affect(self, listener):
        listener.tr_replace(self)


class ChangeType(Transaction):

    def reverse(self):
        new, old = self.old, self.new
        return ChangeType(self.old, old=self.new, sender=self.sender, target=self.target)

    def proposed_value(self, naked_instance):
        assert naked_instance == self.old
        return self.new

    def do(self, rvalue):
        for c in rvalue.children[:]:
            tree.disconnect(c, rvalue)
        rvalue.naked_instance = self.new
        if hasattr(self.new, 'rnode'):
            rnode = self.new.rnode
            for p in rnode.parents[:]:
                tree.connect(rvalue, p) 
                tree.disconnect(rnode, p)
            for c in rnode.children[:]:
                c.unstable = True
                tree.connect(c, rvalue) 
                tree.disconnect(c, rnode)
                c.unstable = False
                c.invariant()
            self.new.rnode= rvalue
        else:
            active_type = self.target.rtype.get_active(self.new)
            vIndex      = b.wrap_value(active_type, self.new)
            tree.connect(vIndex, rvalue)
            #for vIndex in vIndexes:
                #tree.connect(vIndex, rvalue)
        #==
        if isinstance(rvalue.naked_instance, list):
            assert len(rvalue.children[0].children)==len(rvalue.naked_instance)
        #==
        rvalue._invariant()

    def affect(self, listener):
        listener.tr_change_type(self)


class InsertItem(Transaction):
    def reverse(self):
        idx, val = self.new
        new_old  = self.old[:]
        new_old.insert(idx, val)
        new      = idx
        return RemoveItem(new, new_old, sender= self.sender, target=self.target)

    def proposed_value(self, naked_instance):
        idx, val = self.new
        proposed_value = naked_instance[:]
        proposed_value.insert(idx, val)
        return proposed_value

    def do(self, rvalue):
        #==
        assert rvalue.rtype.is_multi_list()
        tIndex   = rvalue.rtype.children[0]
        idx, val = self.new
        vIndexes = b.wrap_name(tIndex, [val])
        rvalue.naked_instance.insert(idx, val) #modification in place
        assert len(vIndexes) == 1
        vIndex     = vIndexes[0]
        vIndex.idx = idx
        for c in rvalue.children[idx:]:
            c.idx += 1
        tree.connect(vIndex, rvalue)
        #==
        assert len(rvalue.children)==len(rvalue.naked_instance)

    def affect(self, listener):
        listener.tr_insert(self)


class AppendItem(Transaction):
    def reverse(self):
        new_old  = self.old[:]
        new_old.append(self.new)
        new =  -1
        return RemoveItem(new, old=new_old, sender=self.sender, target=self.target)

    def proposed_value(self, naked_instance):
        assert naked_instance == self.old
        proposed_value = naked_instance[:]
        proposed_value.append(self.new)
        return proposed_value

    def do(self, rvalue):
        #==
        assert rvalue.rtype.is_multi_list()
        n        = len(rvalue.naked_instance)
        tIndex   = rvalue.rtype.children[0]
        vIndexes = b.wrap_name(tIndex, [self.new])
        rvalue.naked_instance.append(self.new) #modification in place
        for vIndex in vIndexes:
            vIndex.idx = vIndex.idx + n
            tree.connect(vIndex, rvalue)
        #==
        assert len(rvalue.children)==len(rvalue.naked_instance)

    def affect(self, listener):
        listener.tr_append(self)


class RemoveItem(Transaction):
    def reverse(self):
        new_old  = self.old[:]
        new      = new_old.pop(self.new)
        return InsertItem((self.new, new), old=new_old, sender=self.sender, target=self.target)

    def proposed_value(self, naked_instance):
        assert naked_instance == self.old
        proposed_value = naked_instance[:]
        del proposed_value[self.new]
        return proposed_value

    def do(self, rvalue):
        # remove obsolete reactive cnxs
        assert rvalue.rtype.is_multi_list()
        c = rvalue.children[self.new]
        tree.disconnect(c, rvalue)
        # modify naked_instance
        del rvalue.naked_instance[self.new]
        # create current reactive cnxs
        rvalue.reorder_indexes()
        #==
        # post_conditions
        assert len(rvalue.children)==len(rvalue.naked_instance)

    def affect(self, listener):
        listener.tr_remove(self)

class Error(object):
    def __init__(self, transaction):
        self.transaction = transaction

    def __nonzero__(self):
        return False

    def __repr__(self):
        return repr(self.transaction)

    def do(self, rvalue):
        pass

    def affect(self, listener):
        listener.tr_error(self)
