from reactive import _type as t


def test_type_declaration1():
    scope = t.Scope(__name__)
    type_int  = t._Type(int, name="int")
    scope.register(type_int)
    same_type = scope.get_type("int")
    assert type_int is same_type

def test_type_declaration2():
    scope = t.Scope(__name__)
    vector3_type   = t.List(name="vec3").add_idxs([t._Type(float), t._Type(float), t._Type(float)])
    scope.register(vector3_type)
    mat3x3_type = t.List(name="mat3x3").add_idxs([scope.get_type("vec3"),
                                                  scope.get_type("vec3"),
                                                  scope.get_type("vec3")])
    scope.register(mat3x3_type)
    mat3x2_type = t.List(name="mat3x2").add_idxs([scope.get_type("vec3"),
                                                  scope.get_type("vec3"),
                                                  scope.get_type("vec3")])
    scope.register(mat3x2_type)
    #==
    assert mat3x3_type.children[0].children[0] is mat3x2_type.children[0].children[0] 
    


def test_list_int():
    a = range(5)
    #==
    tl  = t.List()
    tl.add_idx('multi', t._Type(int))
    #==
    return tl.validate(a)


def test_list_mixed():
    a = [1, 1.0, 'aaa']
    #==
    tl  = t.List()
    #==
    tl.add_idx(0, t._Type(int))
    tl.add_idx(1, t._Type(float))
    tl.add_idx(2, t._Type(str))
    #==
    return tl.validate(a)

def test_class():
    #==
    class Test(object):
        def __init__(self, first=None, second=None):
            self.first = first
            self.second = second
    #==
    a = Test(1, "t")
    #==
    tc = t._Class(Test)
    tc.add_attr("first", t._Type(int))
    tc.add_attr("second", t._Type(str))
    #==
    return tc.validate(a)

def test_union():
    base_union  = t.Union(children=[t._Type(int, name="int"), 
                                    t._Type(str, name="str")])
    #==
    class Test(object):
        def __init__(self, first):
            self.first = first
    #==
    tc = t._Class(Test)
    tc.add_attr("first", base_union)
    #==
    assert tc.validate(Test(0))
    assert tc.validate(Test("11"))
    assert not tc.validate(Test(1.0))

