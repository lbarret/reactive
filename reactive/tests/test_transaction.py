from reactive import _type as t
from reactive import _base as b
import random

from nose import with_setup

from data_structures import tree

def setup():
    global rmodule
    rmodule = t.Scope(__name__)
    #globals()["rmodule"] = t.Scope(__name__)

def teardown():
    #==
    if "rmodule" in globals():
        global rmodule
        del rmodule

class Listener(object):
    def __init__(self):
        self.tr_append      = None
        self.tr_remove      = None
        self.tr_delete      = None
        self.tr_replace     = None
        self.tr_insert      = None
        self.tr_change_type = None

@with_setup(setup, teardown)
def test_union():
    class Shape(b.Reactive):
        ttype = t.Union(children=[t._Type(int), t._Type(float), t._Type(str)])
    s = Shape.create()
    assert isinstance(s, Shape)
    assert hasattr(s, 'ttype')
    assert isinstance(s.ttype, (int,float,str))
    #==
    def callback(tr):
        assert isinstance(s.ttype, (basestring))
        assert isinstance(s.rnode['ttype'].get_value(), basestring)
    listener = Listener()
    listener.tr_change_type = callback
    s.rnode['ttype'].register(listener)
    s.rnode['ttype'].change_type( 'toto',True )

@with_setup(setup, teardown)
def test_pseudo_graph2():
    positive_int = t._Type(lambda : 0, type_validator=lambda x : x >= 0, name="pos_int")
    rmodule.register(positive_int)

    class RandomPosInt(b.Reactive):
        min        = rmodule.get_type("pos_int")
        max        = rmodule.get_type("pos_int")
        stride     = rmodule.get_type("pos_int")

    inv_value = t.Union(children=[  rmodule.Rtype('RandomPosInt'), 
                                    rmodule.get_type("pos_int")], default_idx=1)

    rmodule.register(inv_value, "int_value")


    boolean      = t._Type(bool, name="boolean")
    rmodule.register(boolean)


    class Node(b.Reactive):
        name            = t._Type(str)
        in_slots        = t.List().add_idx('multi', rmodule.Rtype('ISlot'))
        out_slots       = t.List().add_idx('multi', rmodule.Rtype('OSlot'))
        distance        = rmodule.get_type("int_value")
        generate_inside = rmodule.get_type("boolean")

    rmodule.check_promises_connected()

    class OSlot(b.Reactive):
        name   = t._Type(str)
        parent = t.Union(children=[rmodule.Rtype(Node), t.NothingType], default_idx=1)

    class ISlot(b.Reactive):
        name   = t._Type(str)
        parent = t.Union(children=[rmodule.Rtype(OSlot), t.NothingType], default_idx=1)

    rmodule.check_promises_connected()


    def create_islot(name):
        islot = ISlot.create()
        islot.rnode['name'].replace(name, True)
        return islot

    def create_oslot(name):
        oslot = OSlot.create()
        oslot.rnode['name'].replace(name, True)
        return oslot

    def create_node(name, islots_name, oslots_name):
        n = Node.create()
        rnode = n.rnode
        rnode['name'].replace(name, True)
        for name in islots_name:
            rnode['in_slots'].append(create_islot(name), True)
        for name in oslots_name:
            rnode['out_slots'].append(create_oslot(name), True)
        return n

    n1 = create_node("2d::split", ["input"], ["splitted", "remainings"])
    #==
    def callback(tr):
        assert isinstance(n1.distance, (RandomPosInt))
        assert isinstance(n1.distance.min, (int))
        assert isinstance(n1.distance.max, (int))
    listener = Listener()
    listener.tr_change_type = callback
    n1.rnode['distance'].register(listener)
    n1.rnode['distance'].change_type( RandomPosInt.create(),True )
    n1.rnode['distance'].listeners.remove(listener)
    #==
    def callback2(tr):
        assert isinstance(n1.distance, (int))
    listener = Listener()
    listener.tr_change_type = callback2
    n1.rnode['distance'].register(listener)
    n1.rnode['distance'].change_type( positive_int.get_default(),True )


@with_setup(setup, teardown)
def test_pseudo_graph3():
    char = t._Type(lambda : 0, type_validator=lambda x : x >= 0, name="char")
    rmodule.register(char)

    boolean      = t._Type(bool, name="boolean")
    rmodule.register(boolean)


    class Node(b.Reactive):
        name            = t._Type(str)
        in_slots        = t.List().add_idx('multi', rmodule.Rtype('ISlot'))
        out_slots       = t.List().add_idx('multi', rmodule.Rtype('OSlot'))
        rule            = t.Union( children=[
                                                rmodule.Rtype('Splitter'), 
                                                rmodule.Rtype('Repeater'), 
                                                rmodule.Rtype('Upper'), 
                                                rmodule.Rtype('Filter'), 
                                            ])

    class Splitter(b.Reactive):
        separator = rmodule.get_type("char")
        repeat    = rmodule.get_type("boolean")

    class Repeater(b.Reactive):
        times = rmodule.get_type("pos_int")

    class Upper(b.Reactive):
        pass

    class Filter(b.Reactive):
        letter = rmodule.get_type("char")

    rmodule.check_promises_connected()

    class OSlot(b.Reactive):
        name   = t._Type(str)
        parent = t.Union(children=[rmodule.Rtype(Node), t.NothingType], default_idx=1)

    class ISlot(b.Reactive):
        name   = t._Type(str)
        parent = t.Union(children=[rmodule.Rtype(OSlot), t.NothingType], default_idx=1)

    rmodule.check_promises_connected()


    def create_islot(name):
        islot = ISlot.create()
        islot.rnode['name'].replace(name, True)
        return islot

    def create_oslot(name):
        oslot = OSlot.create()
        oslot.rnode['name'].replace(name, True)
        return oslot

    def create_node(name, islots_name, oslots_name):
        n = Node.create()
        rnode = n.rnode
        rnode['name'].replace(name, True)
        for name in islots_name:
            rnode['in_slots'].append(create_islot(name), True)
        for name in oslots_name:
            rnode['out_slots'].append(create_oslot(name), True)
        return n

    n1 = create_node("2d::split", ["input"], ["splitted", "remainings"])
    ##==
    #def callback(tr):
        #assert isinstance(n1.distance, (RandomPosInt))
        #assert isinstance(n1.distance.min, (int))
        #assert isinstance(n1.distance.max, (int))
    #listener = Listener()
    #listener.tr_change_type = callback
    #n1.rnode['distance'].register(listener)
    #n1.rnode['distance'].change_type( RandomPosInt.create(),True )
    #n1.rnode['distance'].listeners.remove(listener)
    ##==
    #def callback2(tr):
        #assert isinstance(n1.distance, (int))
    #listener = Listener()
    #listener.tr_change_type = callback2
    #n1.rnode['distance'].register(listener)
    #n1.rnode['distance'].change_type( positive_int.get_default(),True )


