import _value 
import _type 
from data_structures import tree

import sys

#import labs.tools.logger as log
#log.install_logger()

##=================
## for debug, assert_ is used to debug when in QT
## need to be replaced for debugging
## same for debug_func
#import sys
#if 'assert_' not in sys.modules["__builtin__"].__dict__:
    #def assert_(x):
        #assert(x)
    #sys.modules["__builtin__"].__dict__['assert_'] = assert_
#if 'debug_func' not in sys.modules["__builtin__"].__dict__:
    #def pass_():
        #pass
    #sys.modules["__builtin__"].__dict__['debug_func'] = pass_
##=================


keywords = ('__interface__', '__rmodule__', '__factory__')

class ReactiveMeta(type):
    def __new__(cls, name, bases, dct):
        r_types    = {}
        attrs      = {}
        mods       = {}
        specials   = {}
        for k, v in dct.iteritems():
            #==
            if isinstance(v, tuple):
                v, mod = v
            else:
                mod = None
            #==
            if isinstance(v, (_type._Type, _type.ThisType, _type.Promise)) : 
                r_types[k]  = v
                mods[k]     = mod
            elif k in keywords : 
                specials[k] = v
            else : 
                attrs[k]    = v
        #==
        T = type.__new__(cls, name, bases, attrs)
        #==
        factory = specials.get('__factory__')
        if factory : r = _type._Class(factory)
        else       : r = _type._Class(T)
        #==
        mod_name = dct['__module__']
        mod = sys.modules[mod_name]
        if not hasattr(mod, "rmodule"):
            setattr(mod, "rmodule", _type.Scope(mod_name))
        scope = getattr(mod, "rmodule")
        scope.register(r)
        #==
        for k, v in r_types.iteritems():
            r.add_attr(k, v, mods[k])
        T.rtype = r
        #==
        for node in tree.dfs(r):
            if isinstance(node, _type.ThisType):
                assert len(node.parents) == 1
                parent = node.parents[0]
                idx    = parent.children.index(node)
                assert isinstance(parent, (_type.Name, _type.Union))
                tree.disconnect(node, parent)
                tree.connect(r, parent, idx=idx)
        #==

        return T


    def __init__(cls, name, bases, dct):
        super(ReactiveMeta, cls).__init__(name, bases, dct)


class Reactive(object):
    __metaclass__ = ReactiveMeta
    def __init__(self, **kw):
        self.__dict__.update(kw)
        self.rnode = wrap_value(self.rtype, self)
        self.rnode._invariant()

    def get_ref(self):
        r = _value.Ref(_value.RefName('ref'), self.rtype)
        self.rnode.reparent_to(r)
        return r

    @classmethod
    def create(cls, **kw):
        return cls.rtype.get_default(**kw)

rvalue_d = {}

def wrap_value(attr_type, naked_instance):
    assert not isinstance(attr_type, _type.Name)
    assert isinstance(attr_type, _type._Type)
    if isinstance(attr_type, _type.Union):
        attr_type = attr_type.get_active(naked_instance)
    assert not isinstance(attr_type, _type.Union)
    #==
    if hasattr(naked_instance,"rnode"):
        node = naked_instance.rnode
    elif isinstance(attr_type, _type.List):
        assert (isinstance(naked_instance, list))
        key  = id(naked_instance)
        node = rvalue_d.get(key)
        if not node :
            node = _value.List(attr_type, naked_instance)
            reparent_names(attr_type.children, naked_instance, node)
            rvalue_d[id(naked_instance)] = node
    else:
        node = _value.Value(attr_type, naked_instance)
        children = attr_type.children
        reparent_names(children, naked_instance, node)
    #==
    assert node.rtype       is attr_type
    assert node.get_value() is naked_instance
    return node


def reparent_names(names, naked_instance, parent_value):
    for c in names:
        vNames = wrap_name(c, naked_instance)
        for vName in vNames:
            vName.reparent_to(parent_value)


def wrap_name(attr_name, naked_instance):
    assert isinstance(attr_name, _type.Name)
    assert not isinstance(attr_name, _type._Type)
    assert len(attr_name.children) == 1
    #==
    attr_type = attr_name.children[0]
    if isinstance(attr_name, _type.Index) and attr_name.multi  : 
        assert (isinstance(naked_instance, list))
        nodes = []
        for idx, el in enumerate(naked_instance):
            node = _value.Index(idx, attr_name, attr_type)
            nodes.append(node)
            vValue = wrap_value(attr_type, el)
            vValue.reparent_to(node)
    elif isinstance(attr_name, _type.Name): 
        node = _value.Name(attr_name, attr_type)
        sub_instance = attr_name.extract(naked_instance)
        vValue = wrap_value(attr_type, sub_instance)
        vValue.reparent_to(node)
        nodes = [node]
    return nodes

