from data_structures import tree
import transaction  as t 
from _type import Nothing

class Value(tree.Node):
    def __init__(self, rtype, naked_instance):
        self.rtype = rtype
        self.naked_instance = naked_instance
        assert naked_instance is not None
        tree.Node.__init__(self)
        #debug
        import os
        assert os.environ.get('RLOCK') is None


    def __getitem__(self, key):
        for c in self.children:
            if c.name.name == key:
                return c

    def __json__(self, filter=None):
        if not self.children:
            return self.get_value()
        else:
            result = {}
            for c in self.children:
                if filter and not filter(c):
                    continue
                assert 'unreadable' not in c.name.mods
                k, v = c.__json__(filter=filter)
                result[k] = v
            return result

    def set_value(self, tr):
        assert self._invariant()
        assert (self.naked_instance == tr.old)
        proposed_value = tr.proposed_value(self.naked_instance)
        if not tr.target.rtype.validate(proposed_value) : 
            tr = t.Error(tr); assert False, tr
        tr.do(self)
        assert self._invariant()
        assert tr.target.rtype.validate(self.naked_instance)
        assert self.naked_instance == proposed_value
        #==
        assert self._invariant()
        for p in self.parents:
            p.post_set_value(tr)
        assert self._invariant()
        #==
        if isinstance(tr, t.AppendItem):
            self.children[-1].init_notification()
        if isinstance(tr, t.InsertItem):
            self.children[tr.new[0]].init_notification()
        if isinstance(tr, t.ChangeType):
            self.children[0].init_notification()
        if isinstance(tr, t.Replace):
            self.init_notification()
        #==
        assert self._invariant()

    def _invariant(self):
        if not isinstance(self.get_value(), (str, list, float, int, Nothing)):
            assert ( not self.children or self.get_value().rnode is self.children[0] ) or \
                   ( self.get_value().rnode is self )
        if self.is_list():
            assert len(self.naked_instance) == len(self.children)
        return True

    def replaceT(self, new, sender, target):
        return t.Replace(new, self.naked_instance, sender, target)

    def get_value(self):
        return self.naked_instance

    def is_name(self):
        return False

    def is_list(self):
        return False

    def is_multi_list(self):
        return False

    def is_container(self):
        return self.children != [] 

    def is_union(self):
        return False

    def is_readable(self):
        return self.rtype.readable

    def __repr__(self):
        return "%s (%s)" %(self.get_value(),self.rtype)

    def init_notification(self):
        for c in self.children:
            c.init_notification()

    def name_children(self):
        return [child for child in self.children if child.is_name()]

class UnionValue(Value):
    def __getitem__(self, key):
        return self.children[0][key]

    def rvalue(self):
        return self.children[0]

    def is_container(self):
        return self.children[0].is_container()

    def is_union(self):
        return True


    def replace(self, new, sender):
        return self.parents[0].replace(new, sender)

    def insert(self, idx, val, sender):
        return self.parents[0].insert(idx, val, sender)

    def append(self, new, sender):
        return self.parents[0].append(new, sender)

    def remove(self, new, sender):
        return self.parents[0].remove(new, sender)

    def change_type(self, new, sender):
        return self.parents[0].change_type(new, sender)

    def register(self, listener):
        self.parents[0].register(listener)

    def full_name(self):
        return 'Union'

class List(Value):

    def __getitem__(self, key):
        if not self.rtype.is_multi_list():
            return Value.__getitem__(self, key)
        #==
        c = self.children[key]
        return c

    def replaceT(self, new, sender, target):
        return t.Replace(new, old=self.naked_instance[:], sender=sender, target=target)

    def appendT(self, new, sender, target):
        return t.AppendItem(new, old=self.naked_instance[:], sender=sender, target=target)

    def removeT(self, idx, sender, target):
        return t.RemoveItem(idx, old=self.naked_instance[:], sender=sender, target=target)

    def insertT(self, idx, val, sender, target):
        return t.InsertItem((idx, val), old=self.naked_instance[:], sender=sender, target=target)

    def reorder_indexes(self):
        assert self.rtype.is_multi_list()
        for idx, el in enumerate(self.children):
            el.idx = idx

    def get_new_item(self):
        assert self.rtype.is_multi_list()
        return self.rtype.children[0].get_default()

    def is_container(self):
        return True

    def is_list(self):
        return True

    def is_multi_list(self):
        return self.rtype.is_multi_list()


class Name(tree.Node, tree.OneChildMixin):
    def __init__(self, name, rtype):
        self.name        = name
        self.listeners   = []
        self.rtype       = rtype
        tree.Node.__init__(self)
        self.locked      = False
        self.unstable    = False

    def __json__(self, filter=None):
        return self.as_key(), self.rvalue().__json__(filter=filter)

    def __repr__(self):
        return "value.Name: %s %s" %(self.name, self.name.mods if self.name.mods else '')

    def __getitem__(self, key):
        return self.get_only_child()[key]

    def replace(self, new, sender):
        value = self.get_only_child()
        t     = value.replaceT(new, sender, self)
        self.set_value(t)
        return t

    def insert(self, idx, val, sender):
        value = self.get_only_child()
        t = value.insertT(idx, val, sender, self)
        self.set_value(t)
        return t

    def append(self, new, sender):
        value = self.get_only_child()
        t = value.appendT(new, sender, self)
        self.set_value(t)
        return t

    def remove(self, new, sender):
        value = self.get_only_child()
        t = value.removeT(new, sender, self)
        self.set_value(t)
        return t

    def change_type(self, new, sender):
        value = self.get_only_child()
        old   = self.rvalue().naked_instance
        tr    = t.ChangeType(new, old=old, sender=sender, target=self)
        self.set_value(tr)
        return t

    def invariant(self):
        if self.unstable:
            return True
        if not self.children or not self.parents:
            return True
        #==
        assert len(self.parents)==1 
        assert isinstance(self.parents[0], Value)
        assert all(isinstance(c, Value) for c in self.children)
        if isinstance(self.extract(), float):
            assert( self.rvalue().get_value() == self.extract() )
        else:
            assert( self.rvalue().get_value() is self.extract() )
        return True
    
    def extract(self):
        return self.name.extract(self.parents[0].naked_instance)

    def set(self, transaction):
        if isinstance(transaction, (t.Replace, t.ChangeType)):
            self.name.set(self.parents[0].naked_instance, transaction.new)
        else:
            pass
            # !!! Tricky !!!
            # no need for the other case 
            # because list are shared via reference.
            # so they cover insert, remove, append transactions
            # hence processed by the value (aka children[0])

    def register(self, listener):
        if listener not in self.listeners:
            #our_logger.info("%s registering on %s" %(listener, self.full_name()))
            self.listeners.append(listener)
        assert(len(self.listeners) == len(set(self.listeners)))

    def lock(self):
        self.locked = True

    def init_notification(self):
        if "no_init" in self.name.mods:
            return
        #==
        tr = t.Init(self.get_value(), sender=self, target=self)
        self.fire_notification(tr)
        for c in self.children:
            c.init_notification()

    def fire_notification(self, transaction):
        impact_str = []
        for l in self.listeners:
            if l is transaction.sender:
                continue
            impact_str.append(" "*30+"----> %s" %(l))
            transaction.affect(l)
        #our_logger.info("%s impact :\n" %(transaction) + "\n".join(impact_str))


    def set_value(self, transaction):
        assert self.invariant()
        child = self.rvalue()
        child.set_value(transaction)
        
    def post_set_value(self, transaction):
        if not isinstance(transaction, t.Error):
            self.set(transaction)
        assert self.invariant() # no warranty once we call notify
        assert self.rvalue()._invariant()
        #==
        self.fire_notification(transaction)

    def get_value(self):
        child     = self.get_only_child()
        result    = child.get_value()
        assert self.invariant()
        return result

    def rvalue(self):
        return self.get_only_child()

    def is_name(self):
        return True

    def is_union(self):
        return self.rtype.is_union()

    def is_index(self):
        return False

    def as_key(self):
        return self.name.name

    def full_name(self):
        parents = [str(p.as_key()) for p in tree.walk_up(self, []) if p.is_name()]
        parents.insert(0, 'root')
        return '/'.join(parents)

class RefName(object):
    def __init__(self, name):
        self.name= name
        self.mods = ['no_init']

class Ref(Name):
    def set(self, transaction):
        pass
    def extract(self):
        assert False
    def init_notification(self):
        assert False

class Index(Name):
    def __init__(self, idx, *args):
        Name.__init__(self, *args)
        self.idx = idx

    def as_key(self):
        return self.idx

    def set(self, transaction):
        if not isinstance(self.get_only_child(), List):
            self.name.set(self.parents[0].naked_instance, transaction.new, idx=self.idx)
        assert self.invariant() # no warranty once we call notify

    def extract(self):
        return self.name.extract(self.parents[0].naked_instance, idx=self.idx)

    def is_index(self):
        return True

    def delete(self, sender=True):
        vlist = self.parents[0]
        name  = vlist.parents[0]
        assert vlist.is_multi_list()
        tr = name.remove( self.idx, sender)
        del_tr = t.Delete(self.idx, tr)
        self.fire_notification(del_tr)

def filter_mods(node):
    if isinstance(node, Name) and 'unreadable' in node.name.mods:
        return False
    return True
